const { ESLint } = require('eslint')
const path = require('path')
module.exports = {
  devServe: {
    open: true,
    proty: 3000,
    Proxy: {
      '/girl': {
        target: 'http://rap2api.taobao.org/app/mock/279460',
        changeOrigin: true, // 是否跨域
        pathRewrite: {
          '^/api': '/api' // 重定向
        }
      }
    }
  },
  lintOnSave: false,
  chainWebpack: 
    : (config) => {
      config.resolve.alias
        .set('@', path.join(__dirname, 'src'))
        .set('@components', path.join(__dirname, 'src/components'))
        .set('@views', path.join(__dirname, 'src/views'))
        .set('@utils', path.join(__dirname, 'src/utils'))
        .set('@mixins', path.join(__dirname, 'src/mixins'))
        .set('@api', path.join(__dirname, 'src/api'))
    }
  }
}
